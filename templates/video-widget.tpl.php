<?php
$count = '';
$count = isset($_POST['count']) ? $_POST['count'] : '';
$video_file = file_load(variable_get('widget_video_fid', ''));
$image_file = file_load(variable_get('widget_image_fid', ''));
$image_path = $image_file->uri;
$img = theme('image_style', array('style_name' => 'video_widget_button', 'path' => $image_path));

$video_path = '';
if (isset($video_file->uri)) :
  $video_path = $video_file->uri;
endif;
$widget_title = variable_get('widget_title', '');
if (!empty($video_path)) {
  ?>

  <?php global $base_url; ?>
  <div class="request-box-video-widget align-box-video-widget">
      <div class="get-call-section-video-widget">    	    
          <div class="getcall_">
              <a href="javascript:void(0)" id="getcall_slide_video_widget" class="getacallnow1-video-widget"> 
                  <?php print $img; ?>
              </a>
          </div>		
      </div>
      <div class="request-box-inner-video-widget">
          <!--request a call back-->
          <div class="co-full-video-widget" id="request-video-widget">
              <div class="closebtn-video-widget hidedesk-video-widget"><a href="javascript:void(0);" class="closePopup1-video-widget"><img src="<?php print $base_url; ?>/<?php print drupal_get_path('module', 'video_widget'); ?>/images/closebtn.png" alt="img"></a></div>
              <?php if (!empty($widget_title)) { ?>
                <div class="hdfc-video-title-wrapper"><h2 class="otp-sub-title-video-widget"><?php print $widget_title; ?></h2></div>
              <?php } ?>      
              <div class="hdfc-video-widget-wrapper"><video id="vid1" class="vid" src="<?php print file_create_url($video_path) ?>" controls></video></div>
              <div id="hdfc-video-view-count"><?php print t('Views: '); ?><?php print variable_get('video_views_count', ''); ?></div>
          </div>
      </div>
  </div>

  <?php
}
else {
  ?>

  <?php global $base_url; ?>
  <div class="request-box-video-widget align-box-video-widget request-box-video-widget-video-not-found">
      <div class="get-call-section-video-widget">    	    
          <div class="getcall_">
              <a href="javascript:void(0)" id="getcall_slide_video_widget" class="getacallnow1-video-widget"> 
                  <img src="<?php print $base_url; ?>/<?php print drupal_get_path('module', 'hdfc_video_widget'); ?>/images/hdfc-video-widget-button.png">
              </a>
          </div>		
      </div>
      <div class="request-box-inner-video-widget">
          <!--request a call back-->
          <div class="co-full-video-widget" id="request-video-widget">
              <div class="closebtn-video-widget hidedesk-video-widget"><a href="javascript:void(0);" class="closePopup1-video-widget"><img src="<?php print $base_url; ?>/<?php print drupal_get_path('module', 'hdfc_video_widget'); ?>/images/closebtn.png" alt="img"></a></div>
              <?php if (!empty($widget_title)) { ?>
                <div class="hdfc-video-title-wrapper"><h2 class="otp-sub-title-video-widget"><?php print $widget_title; ?></h2></div>
              <?php } ?>      
              <div class="hdfc-video-widget-wrapper"><h3 class="hdfc-video-widget-error-message"><?php print t('Video not found.'); ?></h3></div>
          </div>
      </div>
  </div>

<?php } ?>