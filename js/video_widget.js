jQuery(document).ready(function() {

      jQuery('.request-box-video-widget').addClass('align1-video-widget');
      VideoPlayToggele();
      jQuery(document).on('click', '.getacallnow1-video-widget, .closePopup1-video-widget', function () {
        jQuery('.request-box-video-widget').toggleClass('align1-video-widget');
        VideoPlayToggele();
      });

      function VideoPlayToggele() {

        if (jQuery('.request-box-video-widget').hasClass('align1-video-widget')) {

          var video = document.getElementById('vid1');
          if (video != null) {
            video.pause();
            video.currentTime = 0;


            var playPromise = video.play();

            // In browsers that don’t yet support this functionality,
            // playPromise won’t be defined.
            if (playPromise !== undefined) {
              playPromise.then(function () {
                // Automatic playback started!
              }).catch(function (error) {
                // Automatic playback failed.
                // Show a UI element to let the user manually start playback.
                jQuery('video').trigger('play');
              });
            }

          }
        }
        else {

          var video = document.getElementById('vid1');
          if (video != null) {
            video.pause();
          }


        }

      }

      // ajax request for view count.
      jQuery('video').on('ended', function () {

        jQuery.ajax({
          type: 'GET',
          url: 'video-widget-view-count',
          dataType: 'json',
          success: function (data) {
            //console.log('success');

            jQuery('#hdfc-video-view-count').text('Views: ' + data.video_views_count);

          },
          error: function (exception) {
            console.log('error');
          }

        });

      }); // Video ended.

});
