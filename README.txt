CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module is responsible for creating slider widget which showcases video.

REQUIREMENTS
------------

No special requirements

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module will provide a block in the Administrator > Structure >
Block. On block configuration page (admin/structure/block/manage/video_widget/video_block/configure)
configure block with available options.

MAINTAINERS
-----------

Current maintainers:
 * Mayuresh Garate - https://www.drupal.org/u/mayuresh7
 * Shashwat Purav - https://www.drupal.org/u/shashwat-purav
 